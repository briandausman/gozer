import React, { Component } from 'react';
import { AsyncStorage, NativeModules, Text, View } from 'react-native';
import { Button } from 'react-native-material-ui';

import { Mutation } from 'react-apollo';
import { UPDATE_USER } from '../../graphql/mutations';

import NwcInput from '../Common/NwcInput';

export default class UpdatePassword extends Component {
  state = {
    password: null,
    newPassword: null,
  };

  updateUser =  async (updateUser, email, password, newPassword) => {
    updateUser({ variables: { email, password, newPassword } })
      .then(async (res) => {
        if( res.data.updateUser) {
          await AsyncStorage.removeItem('auth', () => {
            NativeModules.DevSettings.reload(); // ur logged out, restart the app to start a-new
          });
        }
      })
      .catch((err) => {
        throw new Error(err);
      });
  };

  render() {
    const { emailId } = this.props;
    const { password, newPassword } = this.state;

    return (
      <Mutation mutation={UPDATE_USER} variables={{ emailId, password, newPassword }}>
        {(updateUser, { data }) => (
          <View style={{ width: '100%', borderWidth: 1, padding: 5, borderColor: '#d7d7d7', borderRadius: 4, backgroundColor: '#fff' }}>
            <Text style={{ textAlign: 'center', marginBottom: 10 }}>
              Hello {emailId}, update your password:
            </Text>
            <NwcInput
              secureTextEntry
              autoCapitalize={false}
              autoCorrect={false}
              autoFocus={true}
              defaultValue={null}
              keyboardType="default"
              maxLength={255}
              callback={password => this.setState({ password })}
              placeholder="Old Password"
            />

            <NwcInput
              secureTextEntry
              autoCapitalize={false}
              autoCorrect={false}
              autoFocus={false}
              defaultValue={null}
              keyboardType="default"
              maxLength={255}
              callback={newPassword => this.setState({ newPassword })}
              placeholder="New Password"
            />

            <Button
              accent
              data={data}
              text="Update Password"
              onPress={() => this.updateUser(updateUser, emailId, password, newPassword)}
            />
          </View>
        )}
      </Mutation>
    );
  }
}