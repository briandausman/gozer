// console.disableYellowBox = true;
import React, { Component } from 'react';
import { Alert, AsyncStorage, View } from 'react-native';
import { ApolloProvider, Mutation } from 'react-apollo';
import { COLOR, ThemeContext, getTheme } from 'react-native-material-ui';

// react navigation imports
import MainNavigator from './components/Common/navigation';

// api functions
import client from './graphql/connection';
import { LOGIN } from './graphql/mutations';

// our components
import Login from './components/Login/Login';

// import our styles
import * as styles from './styles/main.css';

const uiTheme = {
  palette: {
    primaryColor: COLOR.deepPurpleA400,
    accentColor: COLOR.pinkA200
  },
  toolbar: {
    container: {
      height: 50,
    },
  },
};


export default class App extends Component {
  // init our state
  state = {
    email: null,
    isAuth: false,
    password: null,
  };

  componentWillMount() {
    this.checkAuth(); // check if logged in
  }

  componentWillUpdate = async () => {
    const isAuth = await AsyncStorage.getItem('auth');
    if (!isAuth) this.setState({ isAuth: false, authorId: null });
  }

  // check for token stored in async storage
  checkAuth = async () => {
    const isAuth = await AsyncStorage.getItem('auth');
    const authorId = await AsyncStorage.getItem('authorId');
    this.setState({ isAuth, authorId });
  }

  // log the user in
  handleLogin = async (login, newState) => {
    const self = this;
    const { email, password } = newState;

    this.setState({ ...newState }, () => {
      login({
        variables: {
          email: email.toLowerCase(),
          password,
        }
      })
        .then(async (res) => {
          if (!res.data.login.token) {
            Alert('There was a problem logging you in.');
            return false;
          }

          // now that we have the token we need to store it inside of the async storage
          await AsyncStorage.setItem('authorId', res.data.login.user.id);

          await AsyncStorage.setItem('auth', res.data.login.token, () => {
            // we also need to log the user in so they can go to the other screens
            self.setState({ isAuth: true, authorId: res.data.login.user.id });
          });

          return res;
        })
        .catch(err => Alert(err));
    });
  }

  render() {
    const {
      authorId,
      email,
      isAuth,
      password,
    } = this.state;

    if (!isAuth) {
      return (
        <ApolloProvider client={client}>
          <Mutation mutation={LOGIN} variables={{ email, password }}>
            {(login, { data }) => (
              <Login login={login} handleLogin={this.handleLogin} returnedData={data} />
            )}
          </Mutation>
        </ApolloProvider>
      );
    }

    return (
      <ApolloProvider client={client}>
        <ThemeContext.Provider value={getTheme(uiTheme)}>
          <View style={{ width: '100%', height: '100%' }}>
            <MainNavigator screenProps={{ s: styles, authorId }} />
          </View>
        </ThemeContext.Provider>
      </ApolloProvider>
    );
  }
}
