## TODOs
Document Better

## Walkthrough This was babsed on
https://www.howtographql.com/graphql-js

## RN Walkthrough
https://blog.brainsandbeards.com/part-2-setting-up-apollo-client-in-a-react-native-app-e766c7e872e2

## Deploy Prisma
`prisma deploy`

## Generate Schema (will run after successful prisma deploy)
`prisma generate`

## Prisma Endpoint (Demo)
HTTP:  https://eu1.prisma.sh/brian-dausman/how-to-graphql-hackernews-clone/dev
WS:    wss://eu1.prisma.sh/brian-dausman/how-to-graphql-hackernews-clone/dev

## Links
Material icons - https://material.io/tools/icons/?icon=3d_rotation&style=baseline
Material colors - https://material.io/tools/color/#!/?view.left=0&view.right=0&primary.color=E040FB
Material components - https://github.com/xotahal/react-native-material-ui/blob/master/docs/Components.md

# NOTES
Login to prisma after creating a server
cd into the prisma directory and run primsa init to create the newest prisma files
prisma deploy to push to the server (you'll now see it in prisma cloud dash as well as the graphql endpoint)

Secret is hardcoded in prisma.yml file as well as the endpoint

https://app.prisma.io/brian-dausman/services/hackernews-prod/hackernews-prod/prod/databrowser
https://hackernews-prod-c18fa0c36e.herokuapp.com/hackernews-prod/prod


{
  "data": {
    "signup": {
      "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjanYyb3cyMnQwMDZrMDc2OTBpMDM3cmxjIiwiaWF0IjoxNTU2NTYyMTA5fQ.HXGwAfQBmh0dZ_oNzvgPx_nRc9bsYGg9u0oSmDQfcc8",
      "user": {
        "id": "cjv2ow22t006k07690i037rlc"
      }
    }
  }
}

# If you make a change to prisma folder
Run prisma deploy, that's about it...

# If you make a change to server folder
You have to redeploy it to heroku:
`git add .`
`git commit -m "Some awesome messgage"`
`git deploy heroku master`


# If you make a change to react-native-boilerplate folder
