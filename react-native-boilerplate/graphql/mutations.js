import gql from 'graphql-tag';

const ADD_POST = gql`
    mutation post($url: String!, $description: String!) {
    post(url: $url, description: $description) {
      id
      url
      description
    }
  }
`;

const DELETE_POST = gql`
  mutation deleteLink($id: ID!) {
    deleteLink(id: $id) {
      id
    }
  }
`;

const LOGIN = gql`
mutation login($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    token
    user {
      id
      email
    }
  }
}
`;

const SIGNUP = gql`
mutation signup($name: String!, $email: String!, $password: String!) {
  signup(name: $name, email: $email, password: $password) {
    token
    user {
      id
      email
      name
    }
  }
}
`;

const UPDATE_POST = gql`
  mutation updateLink($id: ID!, $url: String, $description: String) {
    updateLink(id: $id, data: { url: $url, description: $description }) {
      id
      url
      description
    }
  }
`;

const UPDATE_USER = gql`
  mutation updateUser($email: String!, $password: String!, $newPassword: String!) {
    updateUser(email: $email, password: $password, newPassword: $newPassword) {
      id
      email
      password
    }
  }
`;

export {
  ADD_POST,
  DELETE_POST,
  LOGIN,
  SIGNUP,
  UPDATE_POST,
  UPDATE_USER,
};
