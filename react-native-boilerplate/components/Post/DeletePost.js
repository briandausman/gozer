import React, { Component } from 'react';
import { Button } from 'react-native-material-ui';

import { Mutation } from 'react-apollo';
import { DELETE_POST } from '../../graphql/mutations';

export default class DeletePost extends Component {
  deletePost = (deleteLink, id) => {
    deleteLink({ variables: { id } })
      .then(res => res)
      .catch((err) => {
        throw new Error(err);
      });
  };

  render() {
    const { post } = this.props;

    return (
      <Mutation mutation={DELETE_POST} variables={{ id: post.id }}>
        {(deleteLink, { data }) => (
          <Button
            accent
            data={data}
            text="Delete Post"
            onPress={() => this.deletePost(deleteLink, post.id)}
          />
        )}
      </Mutation>
    );
  }
}
