/*
const text = '#484848';
const border = '#d7d7d7';
const links = '#f233b9';
const linksHover = '#02e2ca';
const primary = '#9012fe';
const primaryTo = '#c212fe';
const cta = '#f20028';
const ctaTo = '#ff0084';
const success = '#85dd33';
const successTo = '#d2ff52';
const warn = '#4dd8ea';
const warnTo = '#33d671';
const error = '#f20028';
const errorTo = '#ff0084';
*/
const zebra = '#f7f7f7';

const authorStyle = {
  marginTop: 15,
  color: '#d7d7d7',
  fontSize: 12,
};

const cardStyle = {
  backgroundColor: '#fff',
  padding: 20,
  width: '96%',
  marginTop: 5,
  borderRadius: 5,
  shadowColor: '#d7d7d7',
  shadowOpacity: 0.2,
};

const description = {
  marginTop: 25,
  marginBottom: 25,
};

const heading = {
  fontWeight: 'bold',
  fontSize: 16,
};

const inputStyle = {
  borderWidth: 1,
  borderColor: zebra,
  padding: 10,
  width: '100%',
  marginBottom: 10,
};

const verticallyAlignedScreen = {
  paddingLeft: '2.5%',
  width: '98.5%',
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#f4f8fb'
};

module.exports = {
  authorStyle,
  cardStyle,
  description,
  heading,
  inputStyle,
  verticallyAlignedScreen
};
