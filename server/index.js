const { GraphQLServer } = require('graphql-yoga');
require('dotenv').config()

const { prisma } = require('../prisma/generated/prisma-client');

const Query = require('./resolvers/Query')
const Mutation = require('./resolvers/Mutation')
const User = require('./resolvers/User')
const Link = require('./resolvers/Link')
const Subscription = require('./resolvers/Subscription')
const Vote = require('./resolvers/Vote')

// the implementation of our schema
const resolvers = {
  Query,
  Mutation,
  Subscription,
  User,
  Link,
  Vote,
};

// setup our server and tell the server what api operations are accepted
const server = new GraphQLServer({
  typeDefs: './server/schema.graphql', // define graphql our schema
  resolvers,
  context: request => {
    return {
      ...request,
      prisma,
    }
  }
});

server.start(() => console.log(`🖖 Server running on port 4000`));