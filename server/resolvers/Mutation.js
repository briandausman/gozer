const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const { APP_SECRET, getUserId } = require('../utils');

async function signup(parent, args, context, info) {
  // 1
  const password = await bcrypt.hash(args.password, 10)
  // 2
  const user = await context.prisma.createUser({ ...args, password })

  // 3
  const token = jwt.sign({ userId: user.id }, APP_SECRET)

  // 4
  return {
    token,
    user,
  }
}

async function login(parent, args, context, info) {
  // 1
  const user = await context.prisma.user({ email: args.email })
  if (!user) {
    throw new Error('No such user found')
  }

  // 2
  const valid = await bcrypt.compare(args.password, user.password)
  if (!valid) {
    throw new Error('Invalid password')
  }

  const token = jwt.sign({ userId: user.id }, APP_SECRET)

  // 3
  return {
    token,
    user,
  }
}

async function updateUser(parent, args, context, info) {
  const user = await context.prisma.user({ email: args.email })
  if (!user) throw new Error('No such user found');

  console.log(args.password, user.password);
  const valid = await bcrypt.compare(args.password, user.password);
  console.log(valid);
  if (!valid) throw new Error('Invalid password');

  const newPassword = await bcrypt.hash(args.newPassword, 10);
  const updatedUser = await context.prisma.updateUser({
    where: { email: args.email },
    data: {
      password: newPassword,
    },
  })
  return updatedUser;
}

function post(parent, args, context, info) {
  console.log(args);
  // const userId = args.postedBy.id;
  const userId = getUserId(context)

  return context.prisma.createLink({
    url: args.url,
    description: args.description,
    postedBy: { connect: { id: userId } },
  })
}

async function deleteLink(parent, args, context, info) {
  // get the users info
  const userId = getUserId(context);

  // get the author of this post
  const postAuthorId = await context.prisma.link({ id: args.id }).postedBy();

  // check to see if post exists
  const post = await context.prisma.link({ id: args.id });

  // if no post, throw error
  if (!post) throw new Error('No Post Found to Delete');

  // see if author matches current user
  const isAuthorized = (postAuthorId.id === userId);

  // if user id does not match post id, throw error
  if (!isAuthorized) throw new Error('This is not your post...');

  // else delete item
  const deletedPost = await context.prisma.deleteLink({ id: args.id });

  //return the deleted item
  return deletedPost;
}

async function updateLink(parent, args, context, info) {
  const link = await context.prisma.link({ id: args.id });
  if (!link) throw new Error('Post not found');

  const updatePost = await context.prisma.updateLink({
      where: {
        id: args.id,
      },
      data: {...args.data},
  }, info);

  return updatePost;
}

/*
async function updateLink(parent, args, context, info) {
  // get the users info
  const userId = getUserId(context);

  // get the author of this post
  const postAuthorId = await context.prisma.link({ id: args.id }).postedBy();

  // check to see if post exists
  const post = await context.prisma.link({ id: args.id });

  // if no post, throw error
  if (!post) throw new Error('No Post Found to Update');

  // see if author matches current user
  const isAuthorized = (postAuthorId.id === userId);

  // if user id does not match post id, throw error
  if (!isAuthorized) throw new Error('This is not your post...');

  // else delete item
  const updatedPost = await context.prisma.updateLink({
    id: args.id,
    url: args.url,
    description: args.description,
  });

  //return the deleted item
  return updatedPost;
}
*/

async function vote(parent, args, context, info) {
  const userId = getUserId(context);

  const linkExists = await context.prisma.$exists.vote({
    user: { id: userId },
    link: { id: args.linkId },
  });

  if (linkExists) throw new Error(`Already voted for link: ${args.linkId}`);

  return context.prisma.createVote({
    user: { connect: { id: userId } },
    link: { connect: { id: args.linkId } },
  });
}

module.exports = {
  signup,
  login,
  updateUser,
  post,
  deleteLink,
  updateLink,
  vote,
}