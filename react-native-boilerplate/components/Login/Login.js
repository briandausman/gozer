import React, { Component } from 'react';
import {
  Alert,
  Button,
  View,
} from 'react-native';

import NwcInput from '../Common/NwcInput';
import Register from './Register';

// styles
import { verticallyAlignedScreen } from '../../styles/main.css';

export default class Login extends Component {
  state = {
    email: null,
    password: null,
    showSignup: false,
  };

  validateLogin = () => {
    const { email, password } = this.state;
    const { handleLogin, login } = this.props;

    // check if email is formatted correctly
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // eslint-disable-line
    if (email === null || email === undefined || email === '' || !re.test(String(email).toLowerCase())) {
      Alert('Please enter a valid email');
      return false;
    }

    // check if password is ok
    if (password === null || password === undefined || password === '' || password.length < 6 || password === 'password') {
      Alert('Please enter a valid password');
      return false;
    }

    // if all good, log them in
    handleLogin(login, this.state);
    return true;
  }

  render() {
    const { email, password, showSignup } = this.state;

    if (showSignup === false) {
      return (
        <View style={verticallyAlignedScreen}>
          <NwcInput
            autoCapitalize={false}
            autoCorrect={false}
            autoFocus={true}
            defaultValue={null}
            keyboardType="email-address"
            maxLength={255}
            callback={newEmail => this.setState({ email: newEmail })}
            placeholder="Email"
          />

          <NwcInput
            autoCapitalize={false}
            autoCorrect={false}
            autoFocus={false}
            defaultValue={null}
            keyboardType="default"
            maxLength={255}
            callback={newPassword => this.setState({ password: newPassword })}
            placeholder="Password"
            secureTextEntry
          />

          <Button
            accessibilityLabel="Click Here To Login"
            color="#841584"
            onPress={this.validateLogin}
            title="Login"
          />

          <Button
            accessibilityLabel="Click Here To Signup"
            onPress={() => this.setState({ showSignup: true })}
            title="Signup"
          />
        </View>
      )
    } else {
      return (
        <Register />
      )
    };
  }
}
