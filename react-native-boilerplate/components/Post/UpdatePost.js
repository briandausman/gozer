import React, { Component } from 'react';
import { View } from 'react-native';
import { Button } from 'react-native-material-ui';

import { Mutation } from 'react-apollo';
import { UPDATE_POST } from '../../graphql/mutations';

import NwcInput from '../Common/NwcInput';

import DeletePost from './DeletePost';

export default class UpdatePost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: props.post.description,
      url: props.post.url,
    }
  }
  updatePost = (updateLink, id) => {
    const { description, url } = this.state;

    if (description !== '' && description !== null && url !== '' && url !== null) {
      updateLink({
        id,
        data: {
          id,
          description,
          url,
        }
      })
        .then(res => console.log(res))
        .catch((err) => {
          console.log(err);
          throw new Error(err);
        });
    } else {
      alert('Please enter valid post data.')
      return false;
    }
  };

  urlCallback = (url) => {
    this.setState({ url: url.toLowerCase() });
  }

  descriptionCallback = (description) => {
    this.setState({ description });
  }

  render() {
    const { post, toggleEditing } = this.props;
    const { description, url } = this.state;

    return (
      <View>
        <NwcInput
          autoCapitalize={false}
          autoFocus
          defaultValue={url || null}
          keyboardType="email-address"
          maxLength={255}
          callback={this.urlCallback}
          placeholder="Enter a URL"
        />

        <NwcInput
          autoCapitalize
          autoFocus
          defaultValue={description || null}
          keyboardType="default"
          maxLength={500}
          callback={this.descriptionCallback}
          placeholder="Description"
        />
        <Mutation mutation={UPDATE_POST} variables={{ id: post.id, url, description }}>
          {(updateLink, { data }) => (
            <Button
              primary
              data={data}
              text="Update Post"
              onPress={() => this.updatePost(updateLink, post.id)}
            />
          )}
        </Mutation>

        <Button
          accent
          text="Cancel"
          onPress={() => toggleEditing()}
        />

        <DeletePost post={post} />
      </View>
    );
  }
}
