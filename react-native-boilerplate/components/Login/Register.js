import React, { Component } from 'react';
import { Button, NativeModules, View } from 'react-native';
import { Mutation } from 'react-apollo';

import { verticallyAlignedScreen } from '../../styles/main.css';
import { SIGNUP } from '../../graphql/mutations';

import NwcInput from '../Common/NwcInput';

export default class Register extends Component {
  state = {
    name: null,
    email: null,
    password: null,
  };

  register = async (signup) => {
    const { name, email, password } = this.state;

    signup({
      variables: {
        name: name,
        email: email.toLowerCase(),
        password,
      }
    })
    .then(async (res) => {
      if (res.data.signup.token) {
        alert('Signup successful, please login.');
        setTimeout(() => {
          NativeModules.DevSettings.reload(); // ur logged out, restart the app to start a-new
        }, 10000);
      }
    })
    .catch(err => alert(err));
  }

  render() {
    const { name, email, password } = this.state;

    return (
      <Mutation mutation={SIGNUP} variables={{ name, email, password }}>
        {(signup, { data }) => (
          <View style={verticallyAlignedScreen}>
            <NwcInput
              autoCapitalize
              autoCorrect={false}
              autoFocus={true}
              defaultValue={null}
              keyboardType="default"
              maxLength={255}
              callback={newName => this.setState({ name: newName })}
              placeholder="Your Name"
            />

            <NwcInput
              autoCapitalize={false}
              autoCorrect={false}
              autoFocus={false}
              defaultValue={null}
              keyboardType="email-address"
              maxLength={255}
              callback={newEmail => this.setState({ email: newEmail })}
              placeholder="Email"
            />

            <NwcInput
              autoCapitalize={false}
              autoCorrect={false}
              autoFocus={false}
              defaultValue={null}
              keyboardType="default"
              maxLength={255}
              callback={newPassword => this.setState({ password: newPassword })}
              placeholder="Password"
              secureTextEntry
            />

            <Button
              accessibilityLabel="Click Here To Register"
              color="#841584"
              onPress={() => this.register(signup)}
              title="Register"
            />
          </View>
        )}
      </Mutation>
    );
  }
}
