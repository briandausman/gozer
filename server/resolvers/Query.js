async function feed(parent, args, context, info) {
  const where = args.filter ? {
    OR: [
      { description_contains: args.filter },
      { url_contains: args.filter },
    ],
  } : {};

  const links = await context.prisma.links({
    where,
    skip: args.skip,
    first: args.fist,
    orderBy: args.orderBy,
  });

  const count = await context.prisma.linksConnection({ where })
  .aggregate()
  .count();

  return {
    links,
    count,
  }
}

async function users(parent, args, context, info) {
  console.log(args.filter);
  const where = args.filter ? {
    OR: [
      { id: args.filter },
      { email: args.filter },
    ],
  } : {};

  return await context.prisma.users({ where });
}

async function posts(parent, args, context, info) {
  const posts = await context.prisma.links({
    where: { authorId: args.id }
  });

  return posts;
}

module.exports = {
  feed,
  posts,
  users,
}