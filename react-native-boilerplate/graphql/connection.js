import { AsyncStorage } from 'react-native';
import { ApolloClient, HttpLink, InMemoryCache } from 'apollo-boost';
import { setContext } from 'apollo-link-context';
import env from 'react-native-config';

const authLink = setContext(async (_, { headers }) => {
  const loginToken = await AsyncStorage.getItem('auth'); // get the users token if logged in
  return {
    headers: {
      ...headers,
      authorization: loginToken ? `Bearer ${loginToken}` : ''
    }
  };
});

const httpLink = new HttpLink({
  uri: env.GRAPHQL_URL,
  headers: {
    authorization: env.PRISMA_TOKEN,
    // in storage or in redux persist, for demonstration purposes we do this like that
  }
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
  onError: (e) => { throw new Error(e); },
});

export default client;
