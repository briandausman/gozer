import React, { Component } from 'react';
import { Button, TextInput, View } from 'react-native';

import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { ADD_POST } from '../../graphql/mutations';

export default class CreatePost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: null,
      description: null,
    };
  }

  // update our main state from a lower component
  createPost = (post, newState) => {
    const { url, description } = newState;
    const { screenProps, navigation } = this.props;

    this.setState({ ...newState }, () => {
      post({
        variables: {
          url,
          description,
          postedBy: screenProps.authorId,
        }
      })
        .then(res => {
          navigation.navigate('Posts');
          return res;
        });
    });
  }

  render() {
    const { description, url } = this.state;
    const { screenProps } = this.props;

    return (
      <Mutation mutation={ADD_POST} variables={{ description, url }} refetchQueries={() => [
        {
          query: gql`query {
            feed{
              count
              links{
                id
                description
                url
                postedBy {
                  id
                  email
                }
              }
            }
          }`,
        },
      ]}>
        {post => (
          <View style={[screenProps.s.verticallyAlignedScreen]}>
            <TextInput
              style={[screenProps.s.inputStyle]}
              onChangeText={newUrl => this.setState({ url: newUrl })}
              value={url || null}
              placeholder="url"
              autoCapitalize="none"
            />

            <TextInput
              style={[screenProps.s.inputStyle]}
              onChangeText={newDescription => this.setState({ description: newDescription })}
              value={description || null}
              placeholder="description"
            />

            <Button
              onPress={() => {
                this.createPost(post, this.state);
                this.setState({ url: '', description: '' });
                // todo: go back to the feed page and refetch queries
              }}
              title="Add post"
            />
          </View>
        )}
      </Mutation>
    );
  }
}
