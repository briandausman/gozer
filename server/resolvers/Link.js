function postedBy(parent, args, context) {
  return context.prisma.link({ id: parent.id }).postedBy()
}

function vote(parent, args, context) {
  return context.prisma.link({ id: parent.id }).votes();
}

module.exports = {
  postedBy,
  vote,
}