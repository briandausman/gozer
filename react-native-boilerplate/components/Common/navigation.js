import React from 'react';
import {
  createBottomTabNavigator,
  createStackNavigator,
  createAppContainer
} from 'react-navigation';

import Icon from 'react-native-vector-icons/FontAwesome';

import { COLOR } from 'react-native-material-ui';
import CreatePost from '../Post/CreatePost';
import PostDetail from '../Post/PostDetail';
import FeedComponent from '../Feed';
import Profile from '../Login/Profile';


const PostStack = createStackNavigator({
  Posts: {
    screen: ({ navigation, screenProps }) => (
      <FeedComponent navigation={navigation} screenProps={screenProps} />
    ),
  },
  PostDetail: {
    screen: ({ navigation, screenProps }) => (
      <PostDetail navigation={navigation} screenProps={screenProps} />
    ),
    navigationOptions: {
      title: 'Detail',
    }
  },
  CreatePost: {
    screen: ({ navigation, screenProps }) => (
      <CreatePost navigation={navigation} screenProps={screenProps} />
    ),
  }
});

const AppNavigator = createBottomTabNavigator({
  Posts: {
    screen: PostStack,
    navigationOptions: {
      title: 'Posts',
      tabBarIcon: ({ focused }) => <Icon name="comment-o" size={20} color={focused ? COLOR.blueGrey900 : COLOR.grey300} />,
    }
  },
  Profile: {
    screen: Profile,
    navigationOptions: {
      title: 'Profile',
      tabBarIcon: ({ focused }) => <Icon name="user-o" size={20} color={focused ? COLOR.blueGrey900 : COLOR.grey300} />,
    }
  },
}, {
  tabBarOptions: {
    activeTintColor: COLOR.blueGrey900,
    inactiveTintColor: COLOR.grey300,
    showIcon: true,
  },
});

const MainNavigator = createAppContainer(AppNavigator);

export default MainNavigator;
