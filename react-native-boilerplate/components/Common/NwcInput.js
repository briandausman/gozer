import React, { Component } from 'react';
import { TextInput } from 'react-native';

export default class NwcInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { autoCapitalize, autoFocus, defaultValue, keyboardType, maxLength, callback, placeholder, secureTextEntry } = this.props;

    return (
      <TextInput
        autoCapitalize={autoCapitalize}
        autoCorrect
        autoFocus={autoFocus}
        defaultValue={defaultValue}
        keyboardType={keyboardType}
        maxLength={maxLength}
        onChangeText={(text) => callback(text)}
        placeholder={placeholder}
        secureTextEntry={secureTextEntry || false}
        style={{
          width: '92%',
          marginLeft: '2%',
          marginRight: '2%',
          backgroundColor: '#fff',
          borderWidth: 1,
          borderColor: '#d7d7d7',
          borderRadius: 3,
          paddingHorizontal: '2%',
          paddingVertical: 10,
          marginBottom: 7,
        }}
      />
    );
  }
}
