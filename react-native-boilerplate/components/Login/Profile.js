import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  Button,
  NativeModules,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { Query } from 'react-apollo';
import gql from 'graphql-tag';

import { COLOR } from 'react-native-material-ui';

import UpdatePassword from './UpdatePassword';

const GET_USER_DATA = gql`
  query USERS($filter: String!){
    users(filter: $filter){
      id
      email
      links {
        id
        description
        url
        postedBy {
          id
          email
        }
      }
    }
  }
`;

handleLogout = async () => {
  await AsyncStorage.removeItem('auth', () => {
    NativeModules.DevSettings.reload(); // ur logged out, restart the app to start a-new
  });
}

const Profile = (props) => (
  <Query query={GET_USER_DATA} variables={{ filter: props.screenProps.authorId }}>
    {({ loading, error, data }) => {
      if( error ) return <View style={[propsscreenProps.s.verticallyAlignedScreen]}><Text>{JSON.stringify(error)}</Text></View>;
      if (data.users ) {
        return (
          <View style={[props.screenProps.s.verticallyAlignedScreen]}>
            <ScrollView style={{ width: '100%', marginTop: 40, marginBottom: 10 }}>
              <Text style={{ textAlign: 'center', fontWeight: 'bold',}}>
                Your Posts
              </Text>
              {data.users && data.users[0].links && data.users[0].links.map((post, i) => (
                <TouchableOpacity
                  style={[props.screenProps.s.cardStyle]}
                  key={`${post.url}${i}`}
                  onPress={() => props.navigation.navigate('PostDetail', { post })}
                >
                  <Text>
                    {post.description}
                  </Text>
                  { (post.postedBy)
                    && <Text style={[props.screenProps.s.authorStyle]}>{`Posted By: ${post.postedBy.email}`}</Text>
                  }
                </TouchableOpacity>
              ))}
            </ScrollView>

            <UpdatePassword emailId={data.users[0].email} />

            <Button
              style={{ marginTop: 20 }}
              onPress={() => this.handleLogout()}
              title="Logout"
            />
          </View>
        )
      }

      return <ActivityIndicator size="large" color={COLOR.pinkA200} style={[props.screenProps.s.verticallyAlignedScreen]} />;
    }}
  </Query>
);

export default Profile;
