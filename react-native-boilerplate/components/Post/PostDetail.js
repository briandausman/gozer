import React, { Component } from 'react';
import { Linking, Text, View } from 'react-native';
import { Button } from 'react-native-material-ui';

import UpdatePost from './UpdatePost';

export default class PostDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEditing: false,
      post: props.navigation.state.params.post
    };
  }

  toggleEditing = () => {
    const { isEditing } = this.state;
    this.setState({ isEditing: !isEditing});
  }

  render() {
    const { screenProps } = this.props;
    const { isEditing, post } = this.state;

    return (
      <View style={[screenProps.s.verticallyAlignedScreen]}>
        { (isEditing !== true) ? (
          <View style={[screenProps.s.verticallyAlignedScreen]}>
            <View style={[screenProps.s.cardStyle]}>
              <Text style={[screenProps.s.heading]}>
                {post.url}
              </Text>
              { post.postedBy
                && (
                  <Text style={[screenProps.s.authorStyle]}>
                    {`Posted by ${post.postedBy.email}`}
                  </Text>
                )
              }

              <Text style={[screenProps.s.description]}>
                {post.description}
              </Text>


              <Button
                primary
                text="Visit Site"
                onPress={() => Linking.openURL(`http://${post.url}`)}
              />
            </View>

            {(post.postedBy && (post.postedBy.id === screenProps.authorId))
              && (
                <Button
                  primary
                  text="Edit Post"
                  onPress={() => this.toggleEditing()}
                />
              )
            }
          </View>
        ) : (
          <UpdatePost toggleEditing={this.toggleEditing} post={post} />
        )}
      </View>
    )
  }
}
