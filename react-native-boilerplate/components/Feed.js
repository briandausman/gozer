import React from 'react';
import {
  ActivityIndicator,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import { FloatingAction } from 'react-native-floating-action';
import Icon from 'react-native-vector-icons/FontAwesome';

import { COLOR } from 'react-native-material-ui';

const feedQuery = gql`
  query {
    feed{
      count
      links{
        id
        description
        url
        postedBy {
          id
          email
        }
      }
    }
  }
`;

const actions = [{
  text: 'Add New Post',
  color: COLOR.pinkA200,
  icon: <Icon name="pencil" size={20} color="white" />,
  name: 'bt_add',
  position: 2
}];


const FeedComponent = graphql(feedQuery)((props) => {
  const { error, feed } = props.data;
  const { screenProps } = props;
  if (error) {
    return (
      <View style={[screenProps.s.verticallyAlignedScreen]}>
        <Text>
          {JSON.stringify(error)}
        </Text>
      </View>
    );
  }

  if (feed) {
    return (
      <View style={[screenProps.s.verticallyAlignedScreen]}>
        <ScrollView>
          {feed.links.map((post, i) => (
            <TouchableOpacity
              style={[screenProps.s.cardStyle]}
              key={`${post.url}${i}`}
              onPress={() => props.navigation.navigate('PostDetail', { post })}
            >
              <Text>
                {post.description}
              </Text>
              { (post.postedBy)
                && <Text style={[screenProps.s.authorStyle]}>{`Posted By: ${post.postedBy.email}`}</Text>
              }
            </TouchableOpacity>
          ))}
        </ScrollView>
        <FloatingAction
          color="#fff"
          textColor="#ff0000"
          floatingIcon={<Icon name="plus" size={20} color={COLOR.pinkA200} />}
          actions={actions}
          onPressItem={() => props.navigation.navigate('CreatePost')}
        />
      </View>
    );
  }

  return <ActivityIndicator size="large" color={COLOR.pinkA200} style={[screenProps.s.verticallyAlignedScreen]} />;
});

export default FeedComponent;
